package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null) || inputNumbers.size()==0)
            throw new CannotBuildPyramidException();
        int inputSize = inputNumbers.size();
        int height = 0;
        for (int i = 1; i < inputNumbers.size(); i++) {
            inputSize -= i;
            height++;
            if (inputSize == 0)
                break;
            if (inputSize < 0)
                throw new CannotBuildPyramidException();
        }
        int width = 2*height-1;

        Collections.sort(inputNumbers);

        int[][] result = new int[height][width];

        int curIndex=0;
        int colIndex = 0;
        for(int rowIndex=0; rowIndex<height;rowIndex++) {
            colIndex = height - rowIndex - 1;
            for (int j = 0; j <= rowIndex; j++) {
                result[rowIndex][colIndex]=inputNumbers.get(curIndex);
                curIndex++;
                colIndex+=2;
            }
        }

        return result;
    }


}
