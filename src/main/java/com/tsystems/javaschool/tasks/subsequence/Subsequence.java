package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        if(x.size()>y.size())
            return false;
        if(x.equals(y))
            return true;
        int index = 0;
        boolean flag = false;

        for (int i = 0; i < x.size(); i++) {
            for (int j = index; j < y.size(); j++) {
                if (x.get(i).equals(y.get(j)) && j >= index) {
                    index = j;
                    flag = true;
                    break;
                }

            }
            if (!flag)
                return false;
            else flag = false;
        }

        return true;
    }
}
