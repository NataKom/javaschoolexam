package com.tsystems.javaschool.tasks.calculator;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    List<String> operators = Arrays.asList("(", ")", "+", "-", "*", "/" );

    public String evaluate(String statement) {
        if(statement == null || statement.isEmpty())
            return null;
        statement = statement.replace(" ", "");
        if(!statement.matches("(([\\*\\/\\-\\+\\(\\)])*([0-9]*\\.?[0-9]+))+"))
            return null;
        List<String> inputString = Separate(statement);
        if(inputString == null)
            return null;
        List<String> notationString = ConvertToReversePolishNotation(inputString);
        if(notationString == null)
            return null;



        return evalResult(notationString);
    }

    private List<String> Separate(String input)
    {
        List<String> res = new ArrayList<String>();
        int pos = 0;
        int openCount = 0, closeCount=0;
        String previousEl = "";
        while (pos < input.length()) {
            String s = "" + input.charAt(pos);
            if (s.equals("("))
                openCount++;
            else if (s.equals(")"))
                closeCount++;
            if (!operators.contains(input.charAt(pos))) {
                if (Character.isDigit(input.charAt(pos))) {
                    for (int i = pos + 1; i < input.length() &&
                            (Character.isDigit(input.charAt(i)) || input.charAt(i) == '.'); i++)
                        s += input.charAt(i);
                } else if ((s.equals("+") || s.equals("-")) && (previousEl.isEmpty() || previousEl.equals("("))) {
                    if (s.equals("+")) {
                        pos += s.length();
                        previousEl = s;
                        continue;
                    } else  if (s.equals("-")) {
                        pos += s.length();
                        if (Character.isDigit(input.charAt(pos)))
                            for (int i = pos; i < input.length() &&
                                    (Character.isDigit(input.charAt(i)) || input.charAt(i) == '.'); i++)
                                s += input.charAt(i);
                        res.add(s);
                    }
                    pos += s.length() - 1;
                    previousEl = s;
                    continue;
                }
//                else if ((s.equals("*") || s.equals("/")) && (previousEl.isEmpty() || previousEl.equals("("))) {
//                    res.add("0");
//                    res.add(s);
//                    pos += s.length();
//                    previousEl = "0";
//                    continue;
//                }
            }
            res.add(s);
            pos += s.length();
            previousEl = s;
        }
        if(openCount!=closeCount)
            return null;
        return res;
    }
    private byte GetPriority(String s)  {
        switch (s)
        {
            case "(":
            case ")":
                return 0;
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            default:
                return 5;
        }
    }

    private List<String> ConvertToReversePolishNotation(List<String> input)
    {
        Stack<String> stack = new Stack<String>();
        List<String> res = new ArrayList<String>();

        int pos = 0;
        while (pos < input.size())
        {
            String s = input.get(pos);
            if(operators.contains(s)&& pos>0 && s.equals(input.get(pos-1)))
                return null;
            if(operators.contains(s)){
                if(s.equals(")")) {
                    if(stack.isEmpty())
                        return  null;
                    String stackElement = stack.pop();
                    while(!stackElement.equals("(") && !stack.isEmpty())
                    {
                        res.add(stackElement);
                        if(stack.isEmpty() && !stackElement.equals("("))
                            return null;
                        stackElement = stack.pop();
                    }
                } else
                if(stack.isEmpty() || s.equals("("))
                    stack.push(s);
                else if(GetPriority(s) > GetPriority(stack.peek()) ) {
                    stack.push(s);
                } else {
                    res.add(stack.pop());
                    stack.push(s);
                }
            }else {
                res.add(s);
            }

            pos++;
        }
        while(!stack.isEmpty())
            res.add(stack.pop());
        return res;
    }

    public String evalResult(List<String> input) {
        Stack<String> stack = new Stack<String>();
        int pos = 0;
        while (pos < input.size()) {
            String s = input.get(pos);
            if (operators.contains(s)) {
                double second = Double.parseDouble(stack.pop());
                double first = Double.parseDouble(stack.pop());
                switch (s) {
                    case "+":
                        stack.push(String.valueOf(first + second));
                        break;
                    case "-":
                        stack.push(String.valueOf(first - second));
                        break;
                    case "*":
                        stack.push(String.valueOf(first * second));
                        break;
                    case "/":
                        if(second == 0)
                            return null;
                        stack.push(String.valueOf(first / second));
                        break;
                }
            } else stack.push(s);
            pos++;
        }
        String result = (String.valueOf(new BigDecimal(Double.parseDouble(stack.pop())).setScale(4, RoundingMode.HALF_EVEN).floatValue()));
        if(result.endsWith(".0"))
            result=result.substring(0,result.length()-2);

        return result;
    }
}
